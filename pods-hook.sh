#!/usr/bin/env bash
# version 0.0.2
ARRAY_COUNT=$(jq -r '. | length-1' $BINDING_CONTEXT_PATH)

if [[ $1 == "--config" ]] ; then
  cat <<EOF
configVersion: v1
kubernetes:
- name: onPodLabelModification
  apiVersion: v1
  kind: Pod
  executeHookOnEvent:
    - Added
  namespace:
    nameSelector:
      matchNames:
        - londo003
EOF
else
  # ignore Synchronization for simplicity
  type=$(jq -r '.[0].type' $BINDING_CONTEXT_PATH)
  if [[ $type == "Synchronization" ]] ; then
    echo Got Synchronization event
    exit 0
  fi

  echo "got event ${ARRAY_COUNT}"
  for IND in `seq 0 $ARRAY_COUNT`
  do
    resourceEvent=`jq -r ".[$IND].watchEvent" $BINDING_CONTEXT_PATH`
    resourceName=`jq -r ".[$IND].object.metadata.name" $BINDING_CONTEXT_PATH`

    echo "resourceEvent ${resourceEvent} name ${resourceName} added"
  done
fi
